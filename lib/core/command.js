// 自定义命令行指令
const action_create = require('./action')
const commandSet = ( program ) => {
    program
        .command('create <project> [other...]')
        // 别名 相当于 --help 和 -h
        .alias('c')
        .description('创建项目')
        .action(action_create)
}
module.exports = commandSet