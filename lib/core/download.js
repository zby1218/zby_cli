// 下载命令的具体执行模块
const download = require('download-git-repo')
const config = require('../../config')
const ora = require('ora')
const chalk = require('chalk')
const frameDownload = (answer, project) => {
    const spinner = ora().start()
    spinner.text = 'downloading...'
    // param1: git仓库http链接 param2: 下载到本地该地址下  
    // param3:以什么样的方式来下载 param4: 回调函数，出错处理
    download('direct:' + config.frameworkUrl[answer.framework],project,{clone:true},(err)=>{
        //下载成功 err 为 undefined 根据此设置行为
        if(!err){
            spinner.succeed(chalk.green('download succeed'))
            console.log(chalk.green(answer.framework + ' has been downloaded successfully'))
            console.log('you can run '+ chalk.yellow('\' cd ' + project + ' \' ') + 'to enter framework\'s file');
            console.log('tap ' + chalk.yellow(' \'npm run dev\'')+ ' to run the framework');
        }
        else{
            spinner.fail(chalk.red('download failed'))
        }
    })
}
module.exports = frameDownload