// 命令行执行的具体操作
const inquirer = require('inquirer')
const config = require('../../config')
const download = require('download-git-repo')
const frameDownload = require('./download')
const action_create = async (project, args) => {
    const answer = await inquirer.prompt([
      {
        type: 'list',
        name: 'framework',
        choices: config.framework,
        message: '请选择所使用的框架'
      }
    ])
    console.log(answer);
    // 下载对应代码 详见download.js
    frameDownload(answer,project)
}
module.exports = action_create