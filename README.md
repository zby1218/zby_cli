## 自建命令行工具

### 构建顺序

- 创建`bin`目录 ,下设js文件
- `npm init`初始化项目，同时根据bin目录设置package.json中的bin字段，初始化项目的名字就是命令行命令
- 在项目root 处使用`npm link`将命令行挂载到电脑中
- bin / .js 文件首行写入`#! /usr/bin/env node` 代表使用环境变量中的node进行运行

### 访问技巧

- bin / .js文件中使用 `process.argv`可以获取到命令行的参数

----

### Commander

> 都使用 process.argv 接受 命令行参数会导致大量重复代码（ if 判断） 此时可以引入 npm 包 `commander` 对命令行参数进行处理

#### 使用

`commander`是npm的一个包，可以解析命令行参数

```javascript
// 下载 npm i --save commander
//使用
const { program } = require('commander')
```

----

#### 对 -- help 进行处理

```javascript
program.parse(process.argv)
```

commander将会自动解析命令行传入的参数，但无法自定义

----

#### 自定义指令接受及处理

##### 创建自定义指令

```javascript
const { program } = require('commander')
program
// 设置create命令，第一个参数被视作project名 后续参数被other接受，统一存放数组中
.command('create <project> [other...]')
// 设置别名 相当于--help 与 -h
.alias('c')
// 对命令添加描述
.description('创建项目')
// 命令所执行的具体操作 参数是一个回调函数
.action((project, args) => {
    console.log(project)
    console.log(args)
})
```

----

#### 逻辑拆分

为防止功能增加后单个文件代码冗长，对代码逻辑进行拆分

- 设置lib / core 存放核心代码
- 将与 help 指令相关的存于/ core / help.js 中
- 将指令创建的逻辑代码存于 / core / command.js 中
- 指令创建时，需要指定指令行为action，将action存于 / core / action.js 中, command 引用 action.js

----

### inquirer

诸如`npm`在使用`npm init`时存在一些交互行为，如要求填入project name，version等，如果没有填入则给定默认的值。此类命令行交互行为可以使用npm包inquirer实现

----

#### 使用

```javascript
const inquirer = require('inquirer')
const action_create = (project, args) => {
    // inquirer设置命令行交互行为
    inquirer.prompt([
      {
        // 选择交互的方式
        type: 'list',
		// 填入内容的名称设置          
        name: 'framwork',
        // 可选择的内容
        choices: ['express','koa','egg'],
        // 命令行展示的信息
        message: '请选择所使用的框架'
      } 
    // 对输入进行的处理
    ]).then(( answer ) => {
        console.log(answer);
    })
}
```

#### 逻辑拆分

- 上述`可选择的内容`如果需要增加选项，则要手动找到`choices`属性，在action行为代码量增加后，并不容易，因此将choices 内容存于config.js中 在原模块进行引入

```javascript
module.exports = {
  framwork: ['express','koa','egg']
}
```



----

#### 出现问题

> 引入inquirer包时 命令行执行报错： code: 'ERR_REQUIRE_ESM' 提示不支持 CommonJS模块导入方法

解决：下载的inquirer版本为8，已经不支持CommonJS语法，更换版本为7的inquirer即可正常执行

----

### download-git-repo

本脚手架希望能根据选择下载对应的内容，下载git远程仓库代码需要使用`download-git-repo`

```javascript
const download = require('download-git-repo')
//参数依次为 git仓库http链接 ，下载到本地的何处路径（会自动创建文件夹）， 以何种方式下载，出错处理 
download('direct:xxxx','./aaa',{clone:true},(err)=>{
	console.log(err)
})
```

- 在脚手架实际构建中，接受了参数 project ，为命令行创建的项目名称。将此作为了download的下载目标路径

----

### ora

在下载代码时希望有交互动画则需要使用npm包ora

> ora在版本6后只支持ECMAScript语法，本脚手架工具使用语法CommonJS，因此需要下载版本5的ora

```javascript
const ora = require('ora')
// ora 需要创建实例后使用
//同时需要调用start方法来开始监听下载行为，否则后续动画不会出现
const spinner = ora().start()
//下载时的文字显示
spinner.text('downloading')
//下载成功时的信息提示
spinner.succeed('download successfully')
//下载失败后的信息提示
spinner.fail('download failed')
```

----

### chalk

如果希望给命令行的语句添加颜色，需要使用npm包chalk

> chalk在版本5后只支持ECMAScript语法，本脚手架工具使用语法CommonJS，因此需要下载版本4的chalk

```javascript
const chalk = require('chalk')
//使用方法比较简单 如下即可将命令行显示语句 lalala 颜色修改为绿色
console.log(chalk.green('lalala'))
```

